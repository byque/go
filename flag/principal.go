package main

import (
	"flag"
	"fmt"
)

func main() {
	opcionPtr := flag.String("opcion", "defecto", "Opción a ejecutar")

	flag.Parse()

	fmt.Println("Opción:", *opcionPtr)
}
