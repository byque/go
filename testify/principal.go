package main

import (
	"fmt"
	"strings"
)

func AMayusculas(texto string) string {
	return strings.ToUpper(texto)
}

func main() {
	texto := "ibm"
	fmt.Println("Este programa convierte un texto a mayúsculas")
	fmt.Println("Ejemplo: ", texto, " a ", AMayusculas(texto))
}
