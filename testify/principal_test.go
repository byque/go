package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAMayusculas(t *testing.T) {
	// Asertar Igualdad
	assert.Equal(t, "CONMEBOL", AMayusculas("conmebol"), "Debería ser CONMEBOL")
}
