# Hola Mundo

Este código muestra los elementos mínimos para crear un programa funcional en
Go. También muestra el nombre del archivo, paquete y la función principal que
pueden ser en español o que tienen que ser en inglés por requerimiento de Go.
