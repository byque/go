# Módulos

Este directorio muestra el módulo `mimodulo` con el archivo de módulos de Go
con el contenido mínimo. Usa el paquete `fmt` de la librería estándar de Go y
el paquete `mipaquete` del directorio actual.

El directorio raíz no tiene que llamarse igual que el módulo, como en este
caso, sin embargo, es recomendable que el directorio se llame igual que el
módulo para incrementar la legibilidad del proyecto.
